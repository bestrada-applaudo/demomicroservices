const express = require('express');
const app = express();
const PORT = 7000;
const eurekaHelper = require('./eureka-helper');

app.listen(PORT, () => {
  console.log("NodeJS service on port 7000");
})

app.get('/message', (req, res) => {
 res.json("I'm a NodeJS service")
})
eurekaHelper.registerWithEureka('nodejs-service', PORT);
