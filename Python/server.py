#!/usr/bin/python3
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
#from sqlalchemy import create_engine
from json import dumps
from flask import Response

#db_connect = create_engine('sqlite:///chinook.db')
app = Flask(__name__)
api = Api(app)


class Health(Resource):
    def get(self):
        return {'status':'UP'}

class Message(Resource):
    def get(self):
        text = "I'm a Python service, add funcionality"
        resp = Response(text, status=200, mimetype='text')
        return resp

api.add_resource(Health, '/health.json') 
api.add_resource(Message, '/message') 


if __name__ == '__main__':
     app.run(host='0.0.0.0', port=3000)
