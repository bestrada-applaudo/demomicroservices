package com.applaudo.microservices.demo.auth.dao;

import com.applaudo.microservices.demo.auth.model.UseUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UseUserDao  extends CrudRepository<UseUser, Long>{
    public UseUser findByUseUserName(String useUserName);
}