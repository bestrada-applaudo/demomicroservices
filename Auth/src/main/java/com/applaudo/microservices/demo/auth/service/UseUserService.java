package com.applaudo.microservices.demo.auth.service;

import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.applaudo.microservices.demo.auth.model.RolRole;
import com.applaudo.microservices.demo.auth.model.UseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import com.applaudo.microservices.demo.auth.dao.UseUserDao;

@Transactional
@Service(value = "useUserService")
public class UseUserService implements UserDetailsService {
    
    @Autowired
    private UseUserDao useUserDao;

    public UserDetails loadUserByUsername(String useUserName) throws UsernameNotFoundException {
        UseUser useUser = useUserDao.findByUseUserName(useUserName);
        if(useUser == null){
            throw new UsernameNotFoundException("Username or password Invalid.");
        }
        Set<GrantedAuthority> grantedAuthorities = getAuthorities(useUser);

        System.out.println(useUser.getUsePassword());

        return new org.springframework.security.core.userdetails.User(useUser.getUseUserName(), useUser.getUsePassword(), grantedAuthorities);
    }
    
    private Set<GrantedAuthority> getAuthorities(UseUser useUser) {
        Set<RolRole> roleByUseId = useUser.getRoles();
        final Set<GrantedAuthority> authorities = roleByUseId.stream().map(rolRole -> new SimpleGrantedAuthority("ROLE_" + rolRole.getRolName().toString().toUpperCase())).collect(Collectors.toSet());

        return authorities;
    }

}
