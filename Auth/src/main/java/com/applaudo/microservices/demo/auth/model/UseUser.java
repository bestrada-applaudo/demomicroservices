package com.applaudo.microservices.demo.auth.model;

import java.util.Set;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="USE_USER")
public class UseUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int useId;
	private String useUserName;
	private String usePassword;
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "RXU_ROLEXUSER", joinColumns =  @JoinColumn(name ="RXU_ID_USE"),inverseJoinColumns= @JoinColumn(name="RXU_ID_ROL"))
    private Set<RolRole> roles;

    public Set<RolRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<RolRole> roles) {
        this.roles = roles;
    }
	
	public int getUseId() {
		return useId;
	}
	public void setUseId(int useId) {
		this.useId = useId;
	}
	public String getUseUserName() {
		return Integer.toString(useId);
	}
	public void setUseUserName(String useUserName) {
		this.useUserName = useUserName;
	}
	public String getUsePassword() {
		return usePassword;
	}
	public void setUsePassword(String usePassword) {
		this.usePassword = usePassword;
	}

}
