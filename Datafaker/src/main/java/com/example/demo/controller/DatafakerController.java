/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.controller;

import com.example.demo.dto.Movie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.constraints.NotNull;

import com.example.demo.service.JavaFaker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


/**
 * @author rmartinez
 */
@RestController
@RequestMapping("/datafaker")
public class DatafakerController {

    @Value("${movies.new}")
    @NotNull
    private String urlMovie;

    @GetMapping(value = "/{number}", produces = "application/json")
    public ResponseEntity fillMovies(@PathVariable("number") Integer number) throws Exception {

        RestTemplate resT = new RestTemplate();
        JavaFaker javaFaker = new JavaFaker();
        List<Movie> movies = javaFaker.getInfoDetails(number);

        HashMap<String, String> response = resT.postForObject(urlMovie, movies, HashMap.class);

        return ResponseEntity.ok(response);
    }

}
