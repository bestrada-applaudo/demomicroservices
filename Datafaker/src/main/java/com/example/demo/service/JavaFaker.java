package com.example.demo.service;

import com.example.demo.dto.Movie;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Random;

@Service
public class JavaFaker {


    public ArrayList<Movie> getInfoDetails(int cantMovies){
        Faker faker = new Faker();
        Random rand = new Random();
        ArrayList<Movie> movies = new ArrayList<Movie>();

        for (int i = 0; i < cantMovies; i++){
            Movie movie = new Movie();
            int saga = rand.nextInt(5);
            Random r = new Random();
            int low = 1980;
            int high = 2020;
            int year = r.nextInt(high-low) + low;


            movie.setName(faker.superhero().descriptor() + (saga<2?"":" "+saga));
            movie.setStatus(true);
            movie.setYear(Integer.toString(year));
            movie.setCount(rand.nextInt(100));
            movie.setDescription(faker.lorem().paragraph());
            movies.add(movie);
        }
        return movies;
    }
}
