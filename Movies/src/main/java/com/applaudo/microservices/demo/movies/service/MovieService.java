package com.applaudo.microservices.demo.movies.service;

import com.applaudo.microservices.demo.movies.model.Movie;
import org.bson.types.ObjectId;

import java.util.List;

public interface MovieService {
    public List<Movie> findAll();
    public Movie find(ObjectId id);
    public boolean save(Movie movie);
    public boolean delete(Movie movie);
    public Movie checkStatus(String id, int count);
}
