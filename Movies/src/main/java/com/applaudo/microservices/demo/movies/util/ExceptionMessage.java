package com.applaudo.microservices.demo.movies.util;

import com.applaudo.microservices.demo.movies.dto.ResponseApi;
import org.springframework.http.HttpStatus;

public class ExceptionMessage {
	public static ResponseApi error(Exception e){
		return new ResponseApi(HttpStatus.BAD_REQUEST, Messages.ERROR, e.getMessage());
	}
	
	public static ResponseApi error(String message){
		return new ResponseApi(HttpStatus.BAD_REQUEST, Messages.ERROR, message);
	}
	
	public static ResponseApi success(Object object, String message){
		return new ResponseApi(HttpStatus.OK, Messages.SUCCESS, message, object);
	}
	
	public static ResponseApi success(String message){
		return new ResponseApi(HttpStatus.OK, Messages.SUCCESS, message);
	}
	
}
