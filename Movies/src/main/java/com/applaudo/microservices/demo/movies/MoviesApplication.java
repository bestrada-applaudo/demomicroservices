package com.applaudo.microservices.demo.movies;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.applaudo.microservices.demo.movies.util.Log;

@SpringBootApplication
@EnableDiscoveryClient
public class MoviesApplication {
	private static final Logger logger = LoggerFactory.getLogger(MoviesApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(MoviesApplication.class, args);
		Log.print(logger, "Microservice <Movies> was started");
	}

}
