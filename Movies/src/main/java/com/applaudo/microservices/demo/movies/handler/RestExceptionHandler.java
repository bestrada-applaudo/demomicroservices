package com.applaudo.microservices.demo.movies.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);
   
   @Override
   protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
       Throwable mostSpecificCause = ex.getMostSpecificCause();
       ErrorMessage errorMessage;
       if (mostSpecificCause != null) {
           String exceptionName = "Invalid JSON";
           String message = mostSpecificCause.toString();
           errorMessage = new ErrorMessage(exceptionName, message);
       } else {
           errorMessage = new ErrorMessage(ex.getMessage());
       }
       return new ResponseEntity<Object>(errorMessage, headers, status);
   }

}