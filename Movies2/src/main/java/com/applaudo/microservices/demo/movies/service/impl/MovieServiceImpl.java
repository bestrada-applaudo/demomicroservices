package com.applaudo.microservices.demo.movies.service.impl;

import com.applaudo.microservices.demo.movies.dao.MovieRepository;
import com.applaudo.microservices.demo.movies.model.Movie;
import com.applaudo.microservices.demo.movies.service.MovieService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie find(ObjectId id) {
        return movieRepository.findBy_id(id);
    }

    @Override
    public boolean save(Movie movie) {
        try{
            movieRepository.save(movie);
            return true;
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public boolean delete(Movie movie) {
        try{
            movieRepository.delete(movie);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public Movie checkStatus(String id, int count) {
        return movieRepository.checkStatus(id, count);
    }
}
