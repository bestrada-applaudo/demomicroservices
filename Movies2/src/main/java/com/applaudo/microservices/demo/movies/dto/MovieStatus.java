package com.applaudo.microservices.demo.movies.dto;

public class MovieStatus {
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
