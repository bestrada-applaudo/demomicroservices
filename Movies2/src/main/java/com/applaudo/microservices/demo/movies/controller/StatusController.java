package com.applaudo.microservices.demo.movies.controller;

import com.applaudo.microservices.demo.movies.dto.MovieStatus;
import com.applaudo.microservices.demo.movies.model.Movie;
import com.applaudo.microservices.demo.movies.service.MovieService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/check")
public class StatusController {

    @Autowired
    private MovieService movieService;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public MovieStatus status(String id, String count) {
        ObjectId objectId = new ObjectId(id);
        Movie m = movieService.find(objectId);
        int result = m.getCount()-Integer.parseInt(count);
        MovieStatus response = new MovieStatus();

        if(result>=0){
            response.setStatus(true);
        }else{
            response.setStatus(false);
        }

        return response;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded")
    public boolean update(String id, String count) {
        ObjectId objectId = new ObjectId(id);
        Movie m = movieService.find(objectId);
        int result = m.getCount()-Integer.parseInt(count);
        m.setCount(result);
        if(result==0) {
            m.setStatus(false);
        }

        movieService.save(m);

        return true;
    }


}
