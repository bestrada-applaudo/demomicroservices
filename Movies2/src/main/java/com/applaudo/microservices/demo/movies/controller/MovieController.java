package com.applaudo.microservices.demo.movies.controller;

import com.applaudo.microservices.demo.movies.dao.MovieRepository;
import com.applaudo.microservices.demo.movies.dto.MovieStatus;
import com.applaudo.microservices.demo.movies.model.Movie;
import com.applaudo.microservices.demo.movies.service.MovieService;
import com.applaudo.microservices.demo.movies.util.Roles;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@PreAuthorize("hasAuthority('" + Roles.ROLE_ADMIN + "')")
@RestController
@RequestMapping("/movies")
public class MovieController {
	@Autowired
	private MovieService movieService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Movie> getAllMovies() {
		return movieService.findAll();
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Movie getMovieById(@PathVariable("id") ObjectId id) {
		return movieService.find(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void modifyMovieById(@PathVariable("id") ObjectId id, @Valid @RequestBody Movie movie) {
		movie.set_id(id);
		movieService.save(movie);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Movie createMovie(@Valid @RequestBody Movie movie) {
		movie.set_id(ObjectId.get());
		movieService.save(movie);
		return movie;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteMovie(@PathVariable ObjectId id) {
		movieService.delete(movieService.find(id));
	}


}