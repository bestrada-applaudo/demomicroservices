package com.applaudo.microservices.demo.movies.controller;

import com.applaudo.microservices.demo.movies.model.Movie;
import com.applaudo.microservices.demo.movies.service.MovieService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private MovieService movieService;

    @RequestMapping(value = "/movie", method = RequestMethod.GET)
    public Movie getMovieById(String id) {
        ObjectId objectId = new ObjectId(id);
        return movieService.find(objectId);
    }
}
