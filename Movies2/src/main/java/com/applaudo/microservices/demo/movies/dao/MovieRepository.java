package com.applaudo.microservices.demo.movies.dao;

import com.applaudo.microservices.demo.movies.model.Movie;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface MovieRepository extends MongoRepository<Movie, String> {
    public Movie findBy_id(ObjectId _id);

    @Query("{_id: ?0, count :{$gte: ?1}}")
    //findByNameAndCount
    //Movie response = movieService.checkStatus("5daa220f2d35831f18ebfdf5", 4);
    public Movie checkStatus(String id, int count);
}