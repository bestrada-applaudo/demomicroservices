package com.applaudo.microservices.demo.report.util;

import java.sql.Time;
import java.util.Date;


public class Convert {

    public static java.sql.Date sqlToday() {
        Date today = new Date();
        java.sql.Date sqlToday = DateTimeUtil.toSqlDate(today);
        return sqlToday;
    }

    public static Time sqlTime() {
        Date today = new Date();
        Time time = DateTimeUtil.extractTime(today);
        return time;
    }


}
