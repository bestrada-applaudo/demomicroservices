package com.applaudo.microservices.demo.report.controller;

import com.applaudo.microservices.demo.report.dto.*;
import com.applaudo.microservices.demo.report.util.ExceptionMessage;
import com.google.gson.Gson;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/report")
public class ReportController {

	@Value("${movies.report.find}")
	@NotNull
	private String urlMovie;

	@Value("${rent.report.find}")
	@NotNull
	private String urlRent;

/*	@Autowired
	RestTemplate restTemplate;*/


	@GetMapping(value="/find", produces = "application/json")
	//@HystrixCommand(fallbackMethod = "fallbackMethod")
	public String find() throws Exception {
		ResponseEntity<List<Rent>> rateResponse = new RestTemplate().exchange(urlRent, HttpMethod.GET, null, new ParameterizedTypeReference<List<Rent>>() {
		});
		List<Rent> rentList= rateResponse.getBody();

		List<Report> report = new ArrayList<Report>();

		for(Rent r: rentList){
			Report rr = new Report();
			rr.setIdUser(r.getIdUser());
			rr.setFromDate(r.getDate());
			rr.setToDate(r.getReturnDate());


			String idMovie = r.getIdMovie();
			/////////////////////////
			Map<String, String> params = new HashMap<>();
			params.put("id", idMovie);

			String parametrizedArgs = params.keySet().stream().map(k ->
					String.format("%s={%s}", k, k)
			).collect(Collectors.joining("&"));

			Movie m=  new RestTemplate().getForObject(String.format(urlMovie+"?%s", parametrizedArgs), Movie.class, params);
			/////////////////////////
			rr.setMovie(m.getName());
			rr.setYear(m.getYear());
			rr.setDescription(m.getDescription());
			report.add(rr);
		}

		String json = new Gson().toJson(report);

		return json;
	}

	/*@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	public String fallbackMethod(){

		return "Fallback response:: No data available temporarily";
	}*/
}
