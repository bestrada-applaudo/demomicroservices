package com.applaudo.microservices.demo.report;

import com.applaudo.microservices.demo.report.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;


@SpringBootApplication
@EnableDiscoveryClient
@EnableHystrixDashboard
//@EnableCircuitBreaker
public class ReportApplication {
	private static final Logger logger = LoggerFactory.getLogger(ReportApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(ReportApplication.class, args);
		Log.print(logger, "Microservice <Report> was started");
	}

}
