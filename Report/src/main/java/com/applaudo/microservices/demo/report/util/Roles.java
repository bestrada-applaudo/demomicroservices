package com.applaudo.microservices.demo.report.util;

public class Roles {
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_NO_ADMIN = "ROLE_NO_ADMIN";
	public static final String ROLE_OTHERS = "ROLE_OTHERS";
}
