package com.applaudo.microservices.demo.report.util;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil {

    public static String pattern = "yyyy-MM-dd";
    public static String patternFile = "yyyyMMdd";

    public static java.sql.Date toSqlDate(Date date) {

        SimpleDateFormat format = new SimpleDateFormat(pattern);

        Date parsed = null;
        try {
            parsed = (Date) format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        return sql;
    }

    public static java.sql.Time extractTime(Date date){
        java.sql.Time time = new java.sql.Time(date.getTime());
        return 	time;
    }


}
