package com.applaudo.microservices.demo.report.dao;

import com.applaudo.microservices.demo.report.model.Rent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RentDao extends CrudRepository<Rent, Integer> {
    Rent findById(String id);
    void deleteById(int proId);
    List<Rent> findAll();
}
