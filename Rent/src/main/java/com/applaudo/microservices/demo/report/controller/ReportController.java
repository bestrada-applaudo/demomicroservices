package com.applaudo.microservices.demo.report.controller;

import com.applaudo.microservices.demo.report.model.Rent;
import com.applaudo.microservices.demo.report.service.RentService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/report")
public class ReportController {
    @Autowired
    private RentService rentService;

    @RequestMapping(value = "/rent", method = RequestMethod.GET, produces = "application/json")
    public String getAllRent() {

        String json = new Gson().toJson(rentService.findAll());
        return json;
    }
}
