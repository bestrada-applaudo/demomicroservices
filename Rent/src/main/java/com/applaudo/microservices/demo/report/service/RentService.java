package com.applaudo.microservices.demo.report.service;

import com.applaudo.microservices.demo.report.dto.RentMovie;
import com.applaudo.microservices.demo.report.model.Rent;

import java.util.List;

public interface RentService {
    public Rent save(RentMovie rentMovie);
    public List<Rent> findAll();
}
