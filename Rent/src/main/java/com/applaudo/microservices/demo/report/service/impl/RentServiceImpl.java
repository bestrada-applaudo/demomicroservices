package com.applaudo.microservices.demo.report.service.impl;

import java.sql.Time;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import com.applaudo.microservices.demo.report.dao.RentDao;
import com.applaudo.microservices.demo.report.dto.RentMovie;
import com.applaudo.microservices.demo.report.model.Rent;
import com.applaudo.microservices.demo.report.util.Convert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.applaudo.microservices.demo.report.service.RentService;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Transactional
@Service
public class RentServiceImpl implements RentService {
    private static final Logger logger = LoggerFactory.getLogger(RentServiceImpl.class);
    
	@Autowired
	private RentDao rentDao;

	@Value("${movies.check.update}")
	@NotNull
	private String urlPostUpdate;

	@Override
	public Rent save(RentMovie rentMovie) throws RuntimeException{
		Rent r = new Rent();
		r.setIdUser(rentMovie.getIdUser());
		r.setIdMovie(rentMovie.getIdMovie());
		r.setCount(rentMovie.getCount());
		r.setReturnDate(rentMovie.getReturnDate());

		java.sql.Date sqlToday = Convert.sqlToday();
		r.setDate(sqlToday);
		Time time = Convert.sqlTime();
		r.setTime(time);

		rentDao.save(r);
		updateMovie(rentMovie.getIdMovie(), rentMovie.getCount());
		return r;
		
	}

	@Override
	public List<Rent> findAll() {
		return rentDao.findAll();
	}

	private boolean updateMovie(String idMovie, int count){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("id", idMovie);
		map.add("count", Integer.toString(count));

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<String> response = new RestTemplate().postForEntity( urlPostUpdate, request , String.class );

		return Boolean.parseBoolean(response.getBody());
	}
}
