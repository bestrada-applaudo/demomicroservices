package com.applaudo.microservices.demo.report.util;

import java.sql.Timestamp;
import java.util.Date;

public class DateTime {
	public static Timestamp now() {
		Date date= new Date();
		long time = date.getTime();
		Timestamp dateTime = new Timestamp(time);
		
		return dateTime;
	}
}
