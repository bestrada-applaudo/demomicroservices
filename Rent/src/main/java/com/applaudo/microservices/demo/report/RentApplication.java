package com.applaudo.microservices.demo.report;

import com.applaudo.microservices.demo.report.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class RentApplication {
	private static final Logger logger = LoggerFactory.getLogger(RentApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(RentApplication.class, args);
		Log.print(logger, "Microservice <Rent> was started");
	}

}
