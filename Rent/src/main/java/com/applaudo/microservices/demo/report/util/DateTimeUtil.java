package com.applaudo.microservices.demo.report.util;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil {

    public static String pattern = "yyyy-MM-dd";
    public static String patternFile = "yyyyMMdd";

    public static java.sql.Date toSqlDate(Date date) {

        SimpleDateFormat format = new SimpleDateFormat(pattern);

        Date parsed = null;
        try {
            parsed = (Date) format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        return sql;
    }

    public static java.sql.Time extractTime(Date date){
        java.sql.Time time = new java.sql.Time(date.getTime());
        return 	time;
    }

    public static String dateToString(Date date) {
        if(!date.equals(null)) {
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            String dateString = format.format(date);
            return dateString;
        }else{
            return "";
        }

    }

    public static String dateToStringUser(Date date) {
        if(!date.equals(null)) {
            Locale spanishLocale=new Locale("es", "ES");
            SimpleDateFormat format = new SimpleDateFormat("dd-MMMM-yyyy",  spanishLocale);
            String dateString = format.format(date);
            return dateString;
        }else{
            return "";
        }

    }

    public static String dateToStringUser(String date) throws ParseException {
        if(!date.equals(null)) {
            Date dateFinal=new SimpleDateFormat("yyyy-MM-dd").parse(date);

            Locale spanishLocale=new Locale("es", "ES");
            SimpleDateFormat format = new SimpleDateFormat("dd-MMMM-yyyy",  spanishLocale);
            String dateString = format.format(dateFinal);
            return dateString;
        }else{
            return "";
        }

    }

    public static String timeTo24h(Time time) throws ParseException {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm a");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm");
        Date date = parseFormat.parse(time.toString());

        return displayFormat.format(date);
    }

    //
    //yyyy-mm-dd
    public static java.sql.Date stringToDate(String stringDate) throws ParseException {

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = sdf1.parse(stringDate);
        java.sql.Date sqlStartDate = new java.sql.Date(date.getTime());
        return sqlStartDate;
    }

    public static String dateToStringFile(Date date) {
        if(!date.equals(null)) {
            SimpleDateFormat format = new SimpleDateFormat(patternFile);
            String dateString = format.format(date);
            return dateString;
        }else{
            return "";
        }

    }

    public static String timeTo24hFile(Time time) throws ParseException {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HHmmss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss");
        Date date = parseFormat.parse(time.toString());

        return displayFormat.format(date);
    }

}
