package com.applaudo.microservices.demo.report.controller;

import com.applaudo.microservices.demo.report.dto.MovieStatus;
import com.applaudo.microservices.demo.report.dto.RentMovie;
import com.applaudo.microservices.demo.report.dto.ResponseApi;
import com.applaudo.microservices.demo.report.service.RentService;
import com.applaudo.microservices.demo.report.util.ExceptionMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/action")
public class RentController {
	@Autowired
	private RentService rentService;

	@Value("${movies.check.status}")
	@NotNull
	private String urlGetStatus;

	
	/*Sólo usuarios logeados (que no sean admin) pueden alquilar una película, en este caso los "no admin" */
	//@PreAuthorize("hasAuthority('" + Roles.ROLE_NO_ADMIN + "')")
	@PostMapping("/add")
	public ResponseApi add(@RequestBody RentMovie rentMovie) throws Exception {
		try{
			boolean apply = checkMovie(rentMovie.getIdMovie(), rentMovie.getCount());
			//Validar la película en el microservicio de películas
			if(!apply){
				return ExceptionMessage.error("No se puede alquilar la película");
			}

			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String idUser = authentication.getName();
			rentMovie.setIdUser(Integer.parseInt(idUser));

			//System.out.println("====>"+updateMovie(rentMovie.getIdMovie(), rentMovie.getCount()));
			//Restar de la cantidad de películas actuales
			return ExceptionMessage.success(rentService.save(rentMovie), "Película alquilada correctamente");
		}catch(RuntimeException e){
			return ExceptionMessage.error(e);
		}
	}

	private boolean checkMovie(String idMovie, int count){
		Map<String, String> params = new HashMap<>();
		params.put("id", idMovie);
		params.put("count", Integer.toString(count));

		String parametrizedArgs = params.keySet().stream().map(k ->
				String.format("%s={%s}", k, k)
		).collect(Collectors.joining("&"));

		MovieStatus m=  new RestTemplate().getForObject(String.format(urlGetStatus+"?%s", parametrizedArgs), MovieStatus.class, params);

		return m.isStatus();
	}


}
