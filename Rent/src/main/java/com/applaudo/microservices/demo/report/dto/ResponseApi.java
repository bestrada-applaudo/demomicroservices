package com.applaudo.microservices.demo.report.dto;

import org.springframework.http.HttpStatus;

public class ResponseApi {
	private int status;
	private String message;
	private String messageStatus;
	private Object result;
	
	public ResponseApi(HttpStatus status, String message, String messageStatus, Object result){
	    this.status = status.value();
	    this.message = message;
	    this.messageStatus = messageStatus;
	    this.result = result;
    }

    public ResponseApi(HttpStatus status, String message, String messageStatus){
        this.status = status.value();
        this.message = message;
        this.messageStatus = messageStatus;
    }
    
    

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}


    
}
